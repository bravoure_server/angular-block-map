( function () {
    'use strict';

    function blockMap(PATH_CONFIG, NgMap, $timeout, $rootScope, Restangular) {
        return {
            restrict: 'EA',
            replace: true,

            link: function () {

            },

            controller: function ($scope, $controller) {
                // Extend controller
                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                $scope.image = {
                    url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    size: [20, 32],
                    origin: [0,0],
                    anchor: [0, 32]
                };

                var image;

                NgMap.getMap().then(function (map) {
                    $scope.map = map;

                    image = new google.maps.MarkerImage("extend/assets/images/mobile/icons/marker_icon.svg", null, null, null, new google.maps.Size(20,30));

                    $timeout(function (){
                        $scope.initMarkerClusterer();
                    }, 1000);
                });

                $scope.markers = '';

                $scope.initMarkerClusterer = function () {

                    $scope.markers = $rootScope.locations.map(function (location) {
                        return $scope.createMarkerForLocation(location);
                    });

                    var mcOptions = {
                        minimumClusterSize: 3,
                        imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m'
                    };
                    return new MarkerClusterer($scope.map, $scope.markers, mcOptions);
                };

                $scope.createMarkerForLocation = function (location) {

                    var marker = new google.maps.Marker({
                        id: location.id,
                        icon: image,
                        position: new google.maps.LatLng(location.lat, location.lng),
                        title: location.content.title
                    });

                    google.maps.event.addListener(marker, 'click', function (e) {

                        $scope.selectedLocation = location;
                        $scope.map.showInfoWindow('myInfoWindow', this);

                        //$('#accordion_' + location.id).click();

                        /*if ($(window).width() < 1024) {
                            $('body, html').animate({
                                scrollTop: $('#accordion_' + location.id).offset().top - 80
                            }, 1000);
                        }*/
                    });

                    return marker;
                };

                // does the ajax call to retrieve the objects
                Restangular.allUrl('items', host + data.apiVersionAjax + 'locations?lang=' + PATH_CONFIG.current_language).getList()
                    .then(function (items) {

                        $rootScope.locations = items;
                        $rootScope.$emit('locationsOpened', $rootScope.loadedElements);

                        for (var i = 0; i < $rootScope.locations.length; i++ ) {
                            var lat, lng;

                            getLocation(i, $rootScope.locations);
                        }

                    });

                function getLocation (key, obj) {

                    $.ajax({
                        url: 'https://maps.google.com/maps/api/geocode/json?address=' + obj[key].content.address,
                        success: function (response) {
                            $rootScope.locations[key].lat = response.results[0].geometry.location.lat;
                            $rootScope.locations[key].lng = response.results[0].geometry.location.lng;
                        }
                    });
                }

                $rootScope.$on('locationsOpened', function (e, f) {

                    for (var i = 0; i < $scope.markers.length; i ++) {
                        if ($scope.markers[i].id == f.id) {

                            for (var j = 0; j < $rootScope.locations.length; j ++) {
                                if ($rootScope.locations[j].id == f.id) {

                                    $scope.selectedLocation = $rootScope.locations[j];
                                }
                            }

                            $scope.map.showInfoWindow('myInfoWindow', $scope.markers[i]);
                        }
                    }
                });

                $scope.googleMapsUrl = 'https://maps.google.com/maps/api/js?key=AIzaSyBXxBGBjG9M0dZhbCvpa8XHOgMQ_Fy-0_w&libraries=visualization,drawing,geometry,places';

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-map/block-map.html'

        }
    }

    blockMap.$inject = ['PATH_CONFIG', 'NgMap', '$timeout', '$rootScope', 'Restangular'];

    angular
        .module('bravoureAngularApp')
        .directive('blockMap', blockMap);

})();
